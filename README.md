serveur web (NGINX)
Lancer la commande suivante : 
sudo docker run -dp 85:80 --network bridge nginx

Cette commande crée un conteneur NGINX qui est connecté au network bridge. Ici, le but est de simuler un réseau d’application au sein d’une même machine. Nous faisons cela pour permettre à nos différentes applications de communiquer. Avant cette solution, les fichiers de nos conteneurs étaient strictement réservés aux conteneurs. Il était impossible par exemple d'écrire dans les fichiers du conteneur apache depuis l'extérieur du conteneur. Le réseau d'application met un espace commun à toutes les applications qu'il contient, et il devient ainsi beaucoup plus facile de faire échanger des données entre nos apps. 

On peut voir avec la commande docker network ls que le bridge a été créé. Pour plus de détails, on peut lancer docker network inspect bridge qui permet d'avoir le port, l'ip et plein d'autres éléments précis.

On peut aussi créer un réseau avec la commande docker network create nomDuRéseau.

Sur le navigateur à l'adresse localhost:85, vous avez maintenant un joli message Welcome to nginx!


Pour lancer postgresql, phpmyadmin et apache en une seule commande, vous pouvez lancer le script docker (= docker compose) avec la commande :
docker compose up -d

Attention, il faut d'abord se positionner dans le dossier du projet (= virtualisation-docker-compose).

Vous devriez avoir un résultat qui ressemble à cela :

[tech@tech-OptiPlex-3020:~/docker$ sudo docker compose up -d
[+] Building 12.3s (15/15) FINISHED                                                                                                                                                                                          docker:default
 => [web90 internal] load .dockerignore                                                                                                                                                                                                0.0s
 => => transferring context: 2B                                                                                                                                                                                                        0.0s
 => [web90 internal] load build definition from Dockerfile                                                                                                                                                                             0.0s
 => => transferring dockerfile: 544B                                                                                                                                                                                                   0.0s
 => [web90 internal] load metadata for docker.io/library/php:8.2-apache                                                                                                                                                                0.0s
 => [web90  1/11] FROM docker.io/library/php:8.2-apache                                                                                                                                                                                0.0s
 => CACHED [web90  2/11] RUN curl -OL https://github.com/composer/composer/releases/download/2.5.4/composer.phar                                                                                                                       0.0s
 => CACHED [web90  3/11] RUN mv composer.phar /usr/local/bin/composer                                                                                                                                                                  0.0s
 => CACHED [web90  4/11] RUN chmod +x /usr/local/bin/composer && a2enmod rewrite && service apache2 restart                                                                                                                            0.0s
 => CACHED [web90  5/11] RUN apt-get update && apt-get install -y nano                                                                                                                                                                 0.0s
 => CACHED [web90  6/11] RUN apt-get install -y git                                                                                                                                                                                    0.0s
 => CACHED [web90  7/11] RUN git config --global user.email "victorjost34@gmail.com"                                                                                                                                                   0.0s
 => CACHED [web90  8/11] RUN git config --global user.name "VictorJ"                                                                                                                                                                   0.0s
 => CACHED [web90  9/11] RUN apt-get install -y wget                                                                                                                                                                                   0.0s
 => [web90 10/11] RUN docker-php-ext-install pdo_mysql                                                                                                                                                                                 5.4s
 => [web90 11/11] RUN apt-get install -y zip                                                                                                                                                                                           6.6s 
 => [web90] exporting to image                                                                                                                                                                                                         0.3s 
 => => exporting layers                                                                                                                                                                                                                0.3s 
 => => writing image sha256:ec4dc726600b1308864aecdce0d528eaa28df22fa44b94fbfbc46b37ada9427c                                                                                                                                           0.0s 
 => => naming to docker.io/library/docker-web90                                                                                                                                                                                        0.0s 
WARN[0012] Found orphan containers ([pma1 mysql1]) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
[+] Running 4/4
 ✔ Container web90      Started                                                                                                                                                                                                        0.3s 
 ✔ Container postgres1  Running                                                                                                                                                                                                        0.0s 
 ✔ Container web89      Running                                                                                                                                                                                                        0.0s 
 ✔ Container pgadmin1   Running

Et voilà, tout est lancé !

Apache à l'adresse localhost:89
Postgresql se trouve à l'adresse localhost:5432
Pgadmin à l'adresse localhost:83

les identifiants pour pgadmin sont :
admin@example.com
totoLolo

Et postgresql dispose des identifiants suivants:
user: sae
password: totoLolo

avec nom de BD = sae


Ceci marche avec le build. Le build, c'est le dossier web90. C'est en fait un dossier avec un Dockerfile qui permet de se connecter à git. Pourquoi se connecter à git me dirrez-vous ?
Cela nous sert en fait à atteindre la racine des serveurs web en travaillant dans les dossiers web89 et web90, par exemple pour y assurer des sauvegardes ou éditions de code.


En cas d'erreur de type daemon does not exist, assurez vous d'avoir les droits sudo (car il n'est possiblement pas autorisé à lire le document). Si vous êtes ammené à utiliser une fois les droits sudo, vous devrez aussi l'utiliser pour toutes les autres commandes. C'est parce que l'utilisateur varie avec les droits et sans les droits, et mon script est prévu à une utilisation mono-utilisateur. 


Pour toute demande de captures d'écran, cela est difficile dans le readme, c'est pourquoi je vous invite à lancer toutes ces commandes sur une machine linux. 

Si jamais vous souhaitez ne lancer que les serveurs sans la partie build, vous pouvez sur ce repo revenir à l'avant dernier commit (le 'init').
